package arrayminmax;

public class ArrayMinMax {

    public static void main(String[] args) {
      int[] a = new int[10];
    
    ///////////////////////////// Fill array
      for(int i = 0; i<a.length; i++)
      {
          a[i] = (int)(Math.random()*100);
          System.out.print(a[i] + " ");
      }
      
      int min = a[0];
      int minIndex = 0;
      int max = a[0];
      int maxIndex = 0;
      
    /////////////////////////// Find MIN value and its index
      for(int i = 1; i<a.length; i++)
      {
          if (a[i] < min)
          {
              min = a[i];
              minIndex = i;
          }        
      }
      System.out.println("\nMIN value is: a[" + minIndex + "]=" + min);
    /////////////////////////// Find MAX value and its index
      for(int i = 1; i<a.length; i++)
      {
          if (a[i] > max)
          {
              max = a[i];
              maxIndex = i;
          }        
      } 
      System.out.println("MAX value is: a[" + maxIndex + "]=" + max);
            
    }    
    
}
